using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;

// Synchronous Tactile stimulation provided to one foot (either left or right), four limbs or back
public class KTMotionStimulation : MonoBehaviour
{

    // Communication with Neurostimulator
    [DllImport("Dll Pipes Protocol")]
    private static extern int openAndWrite(string message);

    private Stimulator stimulator;

    int CURRENT_BLACK; // was 7 --> BLACK (REHAMOVE3) = YELLOW (KTMOTION)
    int CURRENT_WHITE; // was 7 --> WHITE (REHAMOVE3) = GREY (KTMOTION)
    int CURRENT_BLUE; // was 7
    int CURRENT_RED; // was 7

    int MIN_PW_BLACK;
    int MAX_PW_BLACK;

    int MIN_PW_WHITE;
    int MAX_PW_WHITE;

    int MIN_PW_BLUE;
    int MAX_PW_BLUE;

    int MIN_PW_RED;
    int MAX_PW_RED;

    int frequency;

    public GameObject sessionControl;

    public int flag = 0;
    int start = 0;
    int stop = 0;
    float timer = 0;
    float startTime;
    float stimulationTime = 8f;

    //KT-Motion stimulation fields
    bool isStimulating = false;
    public GameObject stimulationPolicies;
    [HideInInspector] public GaussianStimulation gs;
    [HideInInspector] public CoherentGaussian cg;
    [HideInInspector] public SmoothGaussian sg;
    [HideInInspector] public RampedStimulation rs;
    [HideInInspector] public ConstantStimulation cs;
    public bool calibrated;

    private controlscriptStimulator.Policy stimPolicy;
    controlscriptStimulator controlObject;

    // Start is called before the first frame update
    void Start()
    {
        controlObject = sessionControl.GetComponent<controlscriptStimulator>();
        if (controlObject.enabled) // MovementON session
        {
            stimulator = sessionControl.GetComponent<controlscriptStimulator>().stimulator;
            stimPolicy = sessionControl.GetComponent<controlscriptStimulator>().stimPolicy;

            CURRENT_BLACK = controlObject.CURRENT_BLACK_YELLOW; // was 7
            CURRENT_WHITE = controlObject.CURRENT_WHITE_GREY; // was 7
            CURRENT_BLUE = controlObject.CURRENT_BLUE; // was 7
            CURRENT_RED = controlObject.CURRENT_RED; // was 7

            MIN_PW_BLACK = controlObject.MIN_PW_BLACK_YELLOW;
            MAX_PW_BLACK = controlObject.MAX_PW_BLACK_YELLOW;

            MIN_PW_WHITE = controlObject.MIN_PW_WHITE_GREY;
            MAX_PW_WHITE = controlObject.MAX_PW_WHITE_GREY;

            MIN_PW_BLUE = controlObject.MIN_PW_BLUE;
            MAX_PW_BLUE = controlObject.MAX_PW_BLUE;

            MIN_PW_RED = controlObject.MIN_PW_RED;
            MAX_PW_RED = controlObject.MAX_PW_RED;

            frequency = controlObject.frequency;

            // Initialize CONSTANT parameters
            cs = stimulationPolicies.GetComponent<ConstantStimulation>();
            cs.stimulator = stimulator; 

            // Initialize CONSTANT GAUSSIAN parameters
            gs = stimulationPolicies.GetComponent<GaussianStimulation>();
            gs.stimulator = stimulator; 
            gs.stimulationTime = stimulationTime;

            // Initialize RAMPED STIMULATION parameters
            rs = stimulationPolicies.GetComponent<RampedStimulation>();
            rs.stimulator = stimulator;
            rs.PW = new List<int>(stimulator.PW_min);

            // Initialize COHERENT GAUSSIAN parameters
            cg = stimulationPolicies.GetComponent<CoherentGaussian>();
            cg.stimulator = stimulator;
            cg.PW = new List<int>() { MIN_PW_RED, MIN_PW_BLUE, MIN_PW_BLACK, MIN_PW_WHITE };
            

            // Initialize COHERENT SMOOTH GAUSSIAN parameters
            sg = stimulationPolicies.GetComponent<SmoothGaussian>();
            sg.stimulator = stimulator;

            calibrated = true; //By default, when the stimulation is enabled for the first time  we assume it would be calibrated (has been initialized in control object)
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (controlObject.stopSession == 1)
        {
            startTime = controlObject.illusionTime;
        }

        if (flag == 0)
        {
            if (controlObject.enabled) // true if the script is enabled -> we have a MovementON session (options: 1Limb or 4 Limbs)
            {
                // "l" for left side, "r" for right side, "a" for all limbs, "b" for back
                // ALL LIMBS

                flag = 1; // "flag" controls the whole stimulation
                start = 0;
                stop = 0;
                startTime = Time.time;

            }
        }

        if (flag == 1 & start == 0) // start single TENS wave
        {
            start = 1; // "start" controls the single wave (8s + 2s)
            timer = Time.time;


            // TENS

            if(stimPolicy == controlscriptStimulator.Policy.Constant)
            {
                startConstantStimulation();
            }else if (stimPolicy == (controlscriptStimulator.Policy.ConstantGaussian))
            {
                startGaussianStimulation();
            }
            else if(stimPolicy == controlscriptStimulator.Policy.CoherentRamp)
            {
                startCoherentRampStimulation();
            }
            else if (stimPolicy == controlscriptStimulator.Policy.CoherentGaussian)
            {
                startCoherentGaussianStimulation();
            }
            else
            {
                startSmoothGaussian();
            }

        }

        //When illusion time is finished stop stimulation
        if (Time.time - startTime > controlObject.illusionTime)
        {
            // TENS
            stopStimulation();
            //openAndWrite("0 0 0" + "\0"); // stop stimulation
        }
    }

    private void startConstantStimulation()
    {
        // Enable constant stimulation
        cs.enabled = true;
        if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {
            GeneralFunctions.Start_live_stimulation(controlObject.ser);
            Debug.Log("Start stimulation");
            Thread.Sleep(1000);
            if (!isStimulating) // If we come from a pause in the stimulation only start method is enough without needing to change stimulation phase
            {
                Debug.Log("Change phase");
                GeneralFunctions.Change_program_phase(controlObject.ser, "M0");
                isStimulating = true;
            }
        }

        stimulationPolicies.SetActive(true); //Enable stimulation
    }

    private void startGaussianStimulation()
    {
        // Enable constant gaussian stimulation
        gs.enabled = true;

        
        if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {
            GeneralFunctions.Start_live_stimulation(controlObject.ser);
            Debug.Log("Start stimulation");
            Thread.Sleep(1000);
            if (!isStimulating) // If we come from a pause in the stimulation only start method is enough without needing to change stimulation phase
            {
                Debug.Log("Change phase");
                GeneralFunctions.Change_program_phase(stimulator.ser, "M0");
                isStimulating = true;
            }          
        }

        stimulationPolicies.SetActive(true); //Enable stimulation
        Debug.Log("Tactile started at " + Time.time);

    }

    private void startCoherentRampStimulation()
    {
        //Enable ramped stimulation script
        rs.enabled = true;

        if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {
            // Constant stimulation policy
            stimulationPolicies.GetComponent<CoherentGaussian>().enabled = true;
            GeneralFunctions.Start_live_stimulation(stimulator.ser);
            Debug.Log("Start stimulation");
            Thread.Sleep(1000);
            if (!isStimulating) // If we come from a pause in the stimulation only start method is enough without needing to change stimulation phase
            {
                Debug.Log("Change phase");
                GeneralFunctions.Change_program_phase(stimulator.ser, "M0");
                isStimulating = true;
            }
        }
        stimulationPolicies.SetActive(true); //Enable stimulation
    }

    // This is actually coherent Gaussian stimulation
    private void startCoherentGaussianStimulation()
    {
        // Enable coherent gaussian stimulation
        cg.enabled = true;
        if (stimulator.stim == Stimulator.StimulationDevice.RehaMove3)
        {
            
            stimulationPolicies.SetActive(true);
            // openAndWrite("9 " + MIN_PW_RED.ToString() + " " + "0 " + "\0"); // send pulsewidth constant
            // Thread.Sleep(100);
            // openAndWrite("1 1 1" + "\0"); // start constant stimulation
        }
        else if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {
            
            GeneralFunctions.Start_live_stimulation(stimulator.ser);
            Debug.Log("Start stimulation");
            Thread.Sleep(1000);
            if (!isStimulating) // If we come from a pause in the stimulation only start method is enough without needing to change stimulation phase
            {
                Debug.Log("Change phase");
                GeneralFunctions.Change_program_phase(stimulator.ser, "M0");
                isStimulating = true;
            }
            stimulationPolicies.SetActive(true); //Enable stimulation

        }
    }

    private void startSmoothGaussian()
    {
        // Enable smooth coherent gaussian stimulation
        sg.enabled = true;
        if (stimulator.stim == Stimulator.StimulationDevice.RehaMove3)
        {
            stimulationPolicies.SetActive(true);

        }
        else if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {
            // Smooth Gaussian stimulation policy
           
            GeneralFunctions.Start_live_stimulation(stimulator.ser);
            Debug.Log("Start stimulation");
            Thread.Sleep(1000);
            if (!isStimulating) // If we come from a pause in the stimulation only start method is enough without needing to change stimulation phase
            {
                Debug.Log("Change phase");
                GeneralFunctions.Change_program_phase(stimulator.ser, "M0");
                isStimulating = true;
            }
            stimulationPolicies.SetActive(true); //Enable stimulation

        }

    }

    private void stopStimulation()
    {
        stimulationPolicies.SetActive(false); //Disable stimulation policies because it has finished --> stop stimulation
        MonoBehaviour[] scripts = stimulationPolicies.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = false;
        }

        //Just in case stop stimulation
        if (stimulator.stim == Stimulator.StimulationDevice.RehaMove3)
        {
            openAndWrite("0 0 0" + "\0"); // stop stimulation
        }
        else if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {
            stimulationPolicies.SetActive(false); //Disable stimulation because it has finished --> stop stimulation
            GeneralFunctions.Pause_stimulation(stimulator.ser);
            Debug.Log("Stimulation paused");
        }
    }


}