using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.IO;
using System;
using System.Threading;
using System.Runtime.InteropServices;

public class GaussianStimulation : MonoBehaviour
{
    [DllImport("Dll Pipes Protocol")]
    private static extern int openAndWrite(string message);

    //public GameObject control;

    [HideInInspector] public Stimulator stimulator;

    [HideInInspector] public float stimulationTime = 8f; // stmiulation time
    public float pauseTime = 2f;
    private int stimulate;
    private bool paused;
    [HideInInspector] public bool calibrated;

    [HideInInspector] public List<int> PW_actual = new List<int>() { 10, 10, 10, 10 }; //PW of the current sitmulation
    [HideInInspector] public float timechannel0 = 0f; //for each channnel this takes the time of the stimulation
    [HideInInspector] public float timechannel1 = 0f;
    [HideInInspector] public float timechannel2 = 0f;
    [HideInInspector] public float timechannel3 = 0f;

    private List<float> timechannel;

    // Start is called before the first frame update
    void OnEnable()
    {
        //ser = control.GetComponent<Connection>().ser; //this is the serial port
        timechannel0 = 0f;
        timechannel1 = 0f;
        timechannel2 = 0f;
        timechannel3 = 0f;

        timechannel = new List<float>() { 0, 0, 0, 0 };
        stimulate = 1;
        paused = false;
    }

    // Update is called once per frame
    void Update()
    {
        //stimulation = control.GetComponent<Connection>().flaggaussianstimulation;

        if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {
            for (int i = 0; i < stimulator.stimulationChannels.Count; i++)
            {
                if (stimulator.stimulationChannels[i])
                {
                    if (timechannel[i] < stimulationTime && stimulate == 1)
                    {
                        // Update pulse width so that it follows a gaussian pattern
                        timechannel[i] += Time.deltaTime;
                        PW_actual[i] = (Convert.ToInt32(stimulator.PW_min[i] + (stimulator.PW_max[i] - stimulator.PW_min[i]) * Math.Exp(-1 / 3.0 * Convert.ToDouble(timechannel[i] - stimulationTime / 2) * Convert.ToDouble(timechannel[i] - stimulationTime / 2))));
                        Debug.Log(PW_actual[i]);
                        GeneralFunctions.Select_live_stimulation_parameters(stimulator.ser, stimulator.channels[i], stimulator.PA[i], PW_actual[i]);

                    }
                    else
                    {
                        stimulate = 0;
                        if (timechannel[i] < (stimulationTime + pauseTime))
                        {
                            timechannel[i] += Time.deltaTime;
                            if (!paused)
                            {
                                GeneralFunctions.Pause_stimulation(stimulator.ser);
                                paused = true;
                            }
                        }
                        else
                        {
                            GeneralFunctions.Start_live_stimulation(stimulator.ser);
                            GeneralFunctions.Select_live_stimulation_parameters(stimulator.ser, stimulator.channels[i], stimulator.PA[i], stimulator.PW_min[i]);
                            timechannel[i] = 0f;
                            stimulate = 1;
                            paused = false;
                        }

                    }
                }
            }
        }
        else
        {
            if (stimulator.stim == Stimulator.StimulationDevice.RehaMove3)
            {
                if (timechannel[0] < stimulationTime && stimulate == 1)
                {
                    timechannel[0] += Time.deltaTime;
                    openAndWrite("1 0 0" + "\0"); // start wave stimulation lasting 8s
                }
                else
                {
                    stimulate = 0;
                    if (timechannel[0] < (stimulationTime + pauseTime))
                    {
                        timechannel[0] += Time.deltaTime;
                        if (!paused)
                        {
                            openAndWrite("0 0 0" + "\0"); // stop stimulation
                            paused = true;
                        }
                    }
                    else
                    {
                        timechannel[0] = 0f;
                        stimulate = 1;
                        paused = false;
                    }

                }
            }
        }



    }
}

