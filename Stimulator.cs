using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;

/// <summary>
/// Acts as storage object of the common parameters for the stimulation devices that can be used: Kt-Motion and Rehamove3
/// </summary>
public class Stimulator
{
    public enum StimulationDevice { RehaMove3, KT_Motion };
    /// <summary>
    /// The stimulation device. Takes one of the values of <see cref="StimulationDevice"/>.
    /// </summary>
    public StimulationDevice stim;

    public List<int> channels = new List<int>() { 1, 2, 3, 4 };
    /// <summary>
    /// Active and inactive stimulation channels
    /// </summary>
    public List<bool> stimulationChannels; 

    // Stimulation parameters: amplitude, min and max pulsewidht, frequency

    /// Current amplitude defined for each stimulation channel
    public List<int> PA;
    /// Minimum pulsewidth defined for each stimulation channel
    public List<int> PW_min;
    /// Maximum pulsewidth defined for each stimulation channel
    public List<int> PW_max;
    /// Stimulation frequency
    public int frequency;
    /// Base pulsewidth. Pulsewidth value to decay to when using coherent stimulation
    public int PW_base; 

    /// SerialPort: needed for KT Motion communication
    public SerialPort ser;

    public Stimulator(StimulationDevice device, List<bool> stimulationChannels, List<int> PA, List<int> PW_min, List<int> PW_max, int PW_base, int frequency, SerialPort ser)
    {
        this.stim = device;
        this.stimulationChannels = stimulationChannels;
        this.PA = PA;
        this.PW_min = PW_min;
        this.PW_max = PW_max;
        this.ser = ser;
        this.frequency = frequency;
        this.PW_base = PW_base;
    }
    

}

