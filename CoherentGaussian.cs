using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.IO;
using System;
using System.Threading;
using System.Runtime.InteropServices;

public class CoherentGaussian : MonoBehaviour
{
    [DllImport("Dll Pipes Protocol")]
    private static extern int openAndWrite(string message);

    [HideInInspector] public Stimulator stimulator;

    [HideInInspector] public List<int> PW = new List<int>() { 10, 10, 10, 10};
  
    //[HideInInspector] public float StimulationTime = 8f;

    [HideInInspector] public float timechannel0 = 0f;
    [HideInInspector] public float timechannel1 = 0f;
    [HideInInspector] public float timechannel2 = 0f;
    [HideInInspector] public float timechannel3 = 0f;

    List<float> timechannel;
    //private int updates;

    [Header("PW update parameters")]
    //public int number_updates = 10;
    public float secondsUpdate = 0.01f;
    public int PW_step = 10;

    // Start is called before the first frame update
    void OnEnable()
    {
        //ser = control.GetComponent<Connection>().ser;
        timechannel0 = 0f;
        timechannel1 = 0f;
        timechannel2 = 0f;
        timechannel3 = 0f;

        timechannel = new List<float>() { 0, 0, 0, 0 };
        //updates = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //List<int> readAmplitudes = new List<int>() { 0, 0, 0, 0 };
        /* try
         {
            readAmplitudes = GeneralFunctions.read_channels_PA_LIVE(ser);*/
        if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {

            for (int i = 0; i < stimulator.stimulationChannels.Count; i++)
            {
                if (stimulator.stimulationChannels[i])
                {
                    GeneralFunctions.Select_live_stimulation_parameters(stimulator.ser, stimulator.channels[i], stimulator.PA[i], PW[i]);
                    timechannel[i] += Time.deltaTime;
                    //updates++;
                    //if (updates == number_updates)
                    if (timechannel[i] >= secondsUpdate)
                    {
                        if (PW[i] - PW_step >= stimulator.PW_min[i] - 30)
                        {
                            PW[i] = PW[i] - PW_step;  //Decrease PW to make the tactile illusion more vivid when hitting the target
                        }
                        //updates = 0;
                        timechannel[i] = 0f;
                    }


                }
            }
        }

        else
        {
            openAndWrite("9 " + PW.ToString() + " " + "0 " + "\0"); // send pulsewidth constant
            System.Threading.Thread.Sleep(10);
            openAndWrite("1 1 1" + "\0"); // start constant stimulation
            System.Threading.Thread.Sleep(10);
            //updates++;
            timechannel[0] += Time.deltaTime;
            //if (updates == number_updates)

            if (timechannel[0] >= secondsUpdate)
            {
                if (PW[0] - PW_step >= stimulator.PW_min[0])
                {
                    PW[0] = PW[0] - PW_step; //Ramp down stimulation
                }
                //updates = 0;
                timechannel[0] = 0f;
            }
        }
    }
}