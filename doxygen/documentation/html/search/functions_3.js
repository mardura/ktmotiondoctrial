var searchData=
[
  ['start_5fcalibration_0',['Start_calibration',['../class_general_functions.html#a7afc898c41ea21afe5d5c53670bc9be3',1,'GeneralFunctions']]],
  ['start_5flive_5fstimulation_1',['Start_live_stimulation',['../class_general_functions.html#a795881d32fa00660ab0407ce6f315a11',1,'GeneralFunctions']]],
  ['start_5fpersonalized_5fmodes_2',['Start_personalized_modes',['../class_general_functions.html#aaefbe61d2ed9b6359a08125918a7a8b0',1,'GeneralFunctions']]],
  ['step_5fincrease_5famplitude_3',['Step_increase_amplitude',['../class_general_functions.html#a4bbf5938e500445dfd2c69465f9919fe',1,'GeneralFunctions']]],
  ['step_5fincrease_5fpulsewidth_4',['Step_increase_pulsewidth',['../class_general_functions.html#a3d0845da7f59657e1eb1722ea8980509',1,'GeneralFunctions']]]
];
