var searchData=
[
  ['ser_0',['ser',['../class_stimulator.html#ad2aaa2ffad3a84f64913c75e52552a65',1,'Stimulator']]],
  ['smoothgaussian_1',['SmoothGaussian',['../class_smooth_gaussian.html',1,'']]],
  ['start_5fcalibration_2',['Start_calibration',['../class_general_functions.html#a7afc898c41ea21afe5d5c53670bc9be3',1,'GeneralFunctions']]],
  ['start_5flive_5fstimulation_3',['Start_live_stimulation',['../class_general_functions.html#a795881d32fa00660ab0407ce6f315a11',1,'GeneralFunctions']]],
  ['start_5fpersonalized_5fmodes_4',['Start_personalized_modes',['../class_general_functions.html#aaefbe61d2ed9b6359a08125918a7a8b0',1,'GeneralFunctions']]],
  ['step_5fincrease_5famplitude_5',['Step_increase_amplitude',['../class_general_functions.html#a4bbf5938e500445dfd2c69465f9919fe',1,'GeneralFunctions']]],
  ['step_5fincrease_5fpulsewidth_6',['Step_increase_pulsewidth',['../class_general_functions.html#a3d0845da7f59657e1eb1722ea8980509',1,'GeneralFunctions']]],
  ['stim_7',['stim',['../class_stimulator.html#a192ae6e11e98819796d026995b3e1236',1,'Stimulator']]],
  ['stimulationchannels_8',['stimulationChannels',['../class_stimulator.html#a9d1ef3f8484c2c228fa10036e5bffb0e',1,'Stimulator']]],
  ['stimulator_9',['Stimulator',['../class_stimulator.html',1,'']]]
];
