using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class RehaConstant : MonoBehaviour
{
    [HideInInspector] public int PW;
    [DllImport("Dll Pipes Protocol")]
    private static extern int openAndWrite(string message);
    //public int number_updates = 10;
    public float secondsUpdate = 0.01f;
    public int PW_step = 10;
    //private int updates = 0;
    private float timeChannel = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
        //updates = 0;
        timeChannel = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("RehaMove 3, PW: " + PW);
        openAndWrite("9 " + PW.ToString() + " " + "0 " + "\0"); // send pulsewidth constant
        System.Threading.Thread.Sleep(10);
        openAndWrite("1 1 1" + "\0"); // start constant stimulation
        System.Threading.Thread.Sleep(10);
        //updates++;
        timeChannel += Time.deltaTime;
        //if (updates == number_updates)

        if(timeChannel>= secondsUpdate){
            if (PW >= PW_step)
            {
                PW = PW - PW_step; //Ramp down stimulation
            }
            //updates = 0;
            timeChannel = 0f;
        }
        
    }
}
