using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;

public class SmoothGaussian : MonoBehaviour
{
    [DllImport("Dll Pipes Protocol")]
    private static extern int openAndWrite(string message);

    
    [HideInInspector] public Stimulator stimulator;


    [HideInInspector] public List<int> PW = new List<int>() { 10, 10, 10, 10 };
    [HideInInspector] public List<int> PW_target = new List<int>() { 10, 10, 10, 10 };
    [HideInInspector] public float StimulationTime = 8f;

    [HideInInspector] public float timechannel0 = 0f;
    [HideInInspector] public float timechannel1 = 0f;
    [HideInInspector] public float timechannel2 = 0f;
    [HideInInspector] public float timechannel3 = 0f;

    [HideInInspector] public bool updateFlag = false;

    List<float> timechannel;
    //private int updates;

    [Header("PW update parameters")]
    //public int number_updates = 10;
    public float secondsUpdate = 0.01f;
    public int PW_step = 10;
    public float secondsIncrease = 0.005f;
    public int PW_increase_step = 10;

    // Start is called before the first frame update
    void OnEnable()
    {
        timechannel0 = 0f;
        timechannel1 = 0f;
        timechannel2 = 0f;
        timechannel3 = 0f;

        timechannel = new List<float>() { 0, 0, 0, 0 };
        //updates = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {
            for (int i = 0; i < stimulator.stimulationChannels.Count; i++)
            {
                if (stimulator.stimulationChannels[i])
                {
                    GeneralFunctions.Select_live_stimulation_parameters(stimulator.ser, stimulator.channels[i], stimulator.PA[i], PW[i]);
                    timechannel[i] += Time.deltaTime;
                    if (updateFlag)
                    {
                        if (timechannel[i] >= secondsIncrease)
                        {
                            if (PW[i] + PW_increase_step <= PW_target[i])
                            {
                                PW[i] += PW_increase_step;
                                timechannel[i] = 0f;
                            }
                            else
                            {
                                timechannel[i] = 0f;
                                updateFlag = false;
                            }
                        }
                    }
                    else
                    {
                        if (timechannel[i] >= secondsUpdate)
                        {
                            if (PW[i] - PW_step >= stimulator.PW_min[i] - 30)
                            {
                                PW[i] = PW[i] - PW_step;  //Decrease PW to make the tactile illusion more vivid when hitting the target
                            }
                            timechannel[i] = 0f;
                        }

                    }  

                }
            }
        }
        else
        {
            openAndWrite("9 " + PW[0].ToString() + " " + "0 " + "\0"); // send pulsewidth constant
            System.Threading.Thread.Sleep(10);
            openAndWrite("1 1 1" + "\0"); // start constant stimulation
            System.Threading.Thread.Sleep(10);
            //updates++;
            timechannel[0] += Time.deltaTime;
            //if (updates == number_updates)
            if (updateFlag)
            {
                if (timechannel[0] >= secondsIncrease)
                {
                    if (PW[0] + PW_increase_step <= PW_target[0])
                    {
                        PW[0] += PW_increase_step;
                        timechannel[0] = 0f;
                    }
                    else
                    {
                        timechannel[0] = 0f;
                        updateFlag = false;
                    }
                }
            }
            else
            {
                if (timechannel[0] >= secondsUpdate)
                {
                    if (PW[0] - PW_step >= stimulator.PW_min[0])
                    {
                        PW[0] = PW[0] - PW_step; //Ramp down stimulation
                    }
                    //updates = 0;
                    timechannel[0] = 0f;
                }
            }
        }

    }
}
