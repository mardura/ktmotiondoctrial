
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System;

/// <summary>
/// This class provides a set of static methods required for communication with the KT Motion stimulator (MEDEL Medizinische Elektronik Handelsgesellschaft mbH)
/// </summary>
public static class GeneralFunctions
{
    /// <summary>
    /// Opens the specified serial port to establish communication with the stimulator
    /// </summary>
    /// <param name="ser"> serial port the stimulator is connected to</param>
    public static void OpenConnection(SerialPort ser)
    {
        ser.Open(); // this open the serial port 
        Debug.Log("port was opened succesfully");
    }

    /// <summary>
    /// Closes the specified serial port
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    public static void CloseConnection(SerialPort ser)
    {
        ser.Close(); // this closes the serial port 
        Debug.Log("port was closed succesfully");
    }

    /// <summary>
    /// Enter stimulation mode
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    public static void Start_live_stimulation(SerialPort ser)
    {
        ser.Write("P1\r");
    }

    /// <summary>
    /// Pause stimulation
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    public static void Pause_stimulation(SerialPort ser)
    {
        ser.Write("P0\r");
    }

    /// <summary>
    /// Enter calibration mode
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    public static void Start_calibration(SerialPort ser)
    {
        ser.Write("P2\r");
    }

    /// <summary>
    /// Turn off the stimulator
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    public static void Turn_OFF_stimulator(SerialPort ser)
    {

        ser.Write("OFF\r");
    }

    /// <summary>
    /// Enter personalized mode
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    /// <param name="P_mode"> stimulation P mode:
    /// - P0 for pause/parametrization,
    /// - P1 start live operation(with sensor inside)
    /// - P2 open calibration menu
    /// - P3 start live operation external M(ignore sensor calibration inside)
    /// - P4 start live operation external C(ignore sensor calibration inside) </param>
    public static void Start_personalized_modes(SerialPort ser, string P_mode)
    {
        ser.Write(P_mode + '\r');
    }

    /// <summary>
    /// Change M mode. This is relevant to start stimulation when still on pause after stimulation mode has been entered
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    /// <param name="M_mode">Stimulator M mode (live mode / parametrization mode): 
    /// - M0 change to 1st stimulation phase / 
    /// - M1 change to 2nd stimulation phase / stimulation phase 1
    /// - M2 change to pause phase within program run / stimulation phase 2
    /// </param>
    public static void Change_program_phase(SerialPort ser, string M_mode)
    {
        /*
        :param ser: serial connection
        :param P_mode:
        - P0 for pause/parametrization,
        - P1 start live opersaion (with sensor inside)
        - P2 open calibration menu
        - P3 start live operation external M (ignore sensor calibration inside)
        - P4 start live operation external C (ignore sensor calibration inside) trygitvale
        */
        ser.Write(M_mode + '\r');
    }

    /// <summary>
    /// Increase/decrease amplitude by a unit step
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    /// <param name="channel">Stimulation channel from 1 - 4: 1 (red), 2 (blue), 3 (yellow), 4 (grey)</param>
    /// <param name="increment_decrement"> + for increase, - for decrease. Step size 1mA</param>
    public static void Step_increase_amplitude(SerialPort ser, int channel, string increment_decrement)
    {
        ser.Write("C" + channel.ToString() + increment_decrement + "\r");
    }

    /// <summary>
    /// Increase/decrease pulse width (PW) by a unit step
    /// </summary>
    /// <param name="ser">Serial port the stimulator is connected to</param>
    /// <param name="channel">Stimulation channel from 1 - 4: 1 (red), 2 (blue), 3 (yellow), 4 (grey)</param>
    /// <param name="increment_decrement"> + for increase, - for decrease. Step size 10 uA</param>
    public static void Step_increase_pulsewidth(SerialPort ser, int channel, string increment_decrement)
    {
        /*    :param ser: serial
        :param channel: channel from 1-4 1(red), 2 (blu), 3 (yellow), 4 (grey)
        :param increase: + for increase, - for decrease. step size 10 us
        :return:
        */
        ser.Write("W" + channel.ToString() + increment_decrement + "\r");
    }

    public static void Select_PW(SerialPort ser, int channel, int PW)
    {
        /*
        */
        ser.Write("W" + channel.ToString() + "D" + PW.ToString() + "\r");
    }

    public static void Select_PA(SerialPort ser, int channel, int PA)
    {
        /*
        :param ser: serial
        :param channel: channel from 1-4 1(red), 2 (blu), 3 (yellow), 4 (grey)
        :param PA: absolute value for PA from 0-max defined from step increase
        :return:
        */
        ser.Write("C" + channel.ToString() + "D" + PA.ToString() + "\r");
    }

    public static void Select_F(SerialPort ser, int F)
    {
        /*
        :param ser: serial
        :param channel: channel from 1-4 1(red), 2 (blu), 3 (yellow), 4 (grey)
        :param F: absolute value for F. in P0 (pause) from 1-100
        :return:
        */
        ser.Write("FD" + F.ToString() + "\r");
    }

    public static void Select_live_stimulation_parameters(SerialPort ser, int channel, int PA, int PW)
    {
        /*
        it selects specific PA and PW during live mode. Updates every 125 ms (to be tested)
        :param ser: serial
        :param channel: channel from 1-4 1(red), 2 (blu), 3 (yellow), 4 (grey)
        :param PA: absolute value for PA from 0-max defined from step increase
        :param PW: absolute value for PW from 10-500 us
        :return:
        */
        Select_PW(ser, channel, PW);
        Select_PA(ser, channel, PA);
    }

    public static void Select_PA_max(SerialPort ser, List<int> PA_max, List<int> channels)
    {
        /*
        :param ser: serial
        :param PA_max: Maximum amplitude required during the specific trial
        :param channels: channels that we want to set
        :return:
        */
        bool read = false;
        List<int> read_amplitudes = new List<int>() { 0, 0, 0, 0 };
        while (!read)
        {
            try
            {
                read_amplitudes = read_channels_PA(ser);
                read = true;
            }
            catch (NullReferenceException ex)
            {
                read = false;
            }

        }
        for (int i = 0; i < 4; i++)
        {
            // Increase only until PA_max
            for (int j = 0; j<(PA_max[i]-read_amplitudes[i]); j++)
            {
                Step_increase_amplitude(ser, channels[i], "+");
                System.Threading.Thread.Sleep(50);
            }
            /*
            for (int j = 0; j < PA_max[i]; j++)
            {
                Step_increase_amplitude(ser, channels[i], "+");
                System.Threading.Thread.Sleep(50);
                Debug.Log("step");
            }
            */
        }
        Debug.Log("selected maximum channels amplitude");
    }

    public static void Initialize_stimulation_parameters(SerialPort ser, List<int> channels, List<int> PA_max, List<int> PWs, int F)
    {
        /*
        Function to initialize PA (at the maximum value to be used in live mode), starting PW and F (same for all channels
        :param ser: serial
        :param PA_max: Maximum amplitude required during the specific trial
        :param channels: channels that we want to set
        :return:
        */
        // TODO start everything from zero otherwise if some stimulation was set manually it increments from there --> DONE
        for (int i = 0; i < 4; i++) //4 is th enmaber of channels
        {
            Select_PW(ser, channels[i], PWs[i]);
        }
        Select_PA_max(ser, PA_max, channels);
        Select_F(ser, F);
    }


    //READING FUNCTIONS
    public static string channels_PA_b;
    public static string channels_PW_b;
    public static string channels_PF_b;
    public static string channels_F_b;
    public static int pFrom;
    public static int pTo;
    public static string cutstring;
    public static List<string> uselessstring;
    public static List<int> result;
    public static int resultF;

    public static List<int> read_channels_PA(SerialPort ser) //reading function while the stimulator is in pause
    {
        /*
        :param ser: 
        :return: RETURN A LIST OF INTEGERS CORRESPONDING TO THE AMPLITUDES OF EACH CHANNEL. THIS IS ODNE WITH A DELAY OF 50 MS
        */
        ser.DiscardOutBuffer();
        ser.Write("RC\r");
        try
        {
            ser.ReadExisting();
        }
        catch (TimeoutException)
        {
        }
        System.Threading.Thread.Sleep(50);
        ser.Write("RC\r"); //i MAKE THE READING TWICE, BECAUSE MOST OF THE TIMES THE FIRST ONE IS WRONG       
        try
        {
            channels_PA_b = ser.ReadExisting();
            Debug.Log(channels_PA_b);
            try
            {
                pFrom = channels_PA_b.IndexOf("\"C\":[") + "\"C\":[".Length;
                pTo = channels_PA_b.LastIndexOf("]");
                cutstring = channels_PA_b.Substring(pFrom, (pTo) - (pFrom));
                result = new List<int>(Array.ConvertAll(cutstring?.Split(','), int.Parse));
                Debug.Log(cutstring);
                Debug.Log(result[0]);
                Debug.Log(result[1]);
                Debug.Log(result[2]);
                Debug.Log(result[3]);
                return result;
            }
            catch
            {
                Debug.Log("ERROR IN READING THE STIMULATION PARAMETERS");
                return null;
            }
        }
        catch (TimeoutException)
        {
            Debug.Log("UNABLE TO READ SOMETHING");
            return null;
        }
    }

    public static List<int> read_channels_PW(SerialPort ser) //reading function while the stimulator is in pause
    {
        /*
        :param ser: 
        :return: RETURN A LIST OF INTEGERS CORRESPONDING TO THE PW OF EACH CHANNEL. THIS IS ODNE WITH A DELAY OF 50 MS
        */
        ser.DiscardOutBuffer();
        ser.Write("RW\r");
        try
        {
            ser.ReadExisting();
        }
        catch (TimeoutException)
        {
        }
        System.Threading.Thread.Sleep(50);
        ser.Write("RC\r"); //i MAKE THE READING TWICE, BECAUSE MOST OF THE TIMES THE FIRST ONE IS WRONG       
        try
        {
            channels_PW_b = ser.ReadExisting();
            Debug.Log(channels_PW_b);
            try
            {
                pFrom = channels_PW_b.IndexOf("\"PW\":[") + "\"PW\":[".Length;
                pTo = channels_PW_b.LastIndexOf("]");
                cutstring = channels_PW_b.Substring(pFrom, (pTo) - (pFrom));
                result = new List<int>(Array.ConvertAll(cutstring?.Split(','), int.Parse));
                Debug.Log(cutstring);
                Debug.Log(result[0]);
                Debug.Log(result[1]);
                Debug.Log(result[2]);
                Debug.Log(result[3]);
                return result;
            }
            catch
            {
                Debug.Log("ERROR IN READING THE STIMULATION PARAMETERS");
                return null;
            }
        }
        catch (TimeoutException)
        {
            Debug.Log("UNABLE TO READ SOMETHING");
            return null;
        }
    }


    public static int read_channels_PF(SerialPort ser) //reading function while the stimulator is in pause
    {
        /*
        :param ser: 
        :return:
        */
        ser.DiscardOutBuffer();
        ser.Write("RF\r");
        try
        {
            ser.ReadExisting();
        }
        catch (TimeoutException)
        {
        }
        System.Threading.Thread.Sleep(50);
        ser.Write("RF\r"); //i MAKE THE READING TWICE, BECAUSE MOST OF THE TIMES THE FIRST ONE IS WRONG       
        try
        {
            channels_PF_b = ser.ReadExisting();
            Debug.Log(channels_PF_b);
            try
            {
                pFrom = channels_PF_b.IndexOf("\"F\":") + "\"F\":".Length;
                pTo = channels_PF_b.LastIndexOf("}");
                cutstring = channels_PF_b.Substring(pFrom, (pTo) - (pFrom));
                resultF = int.Parse(cutstring);
                Debug.Log(cutstring);
                Debug.Log(resultF);
                return resultF;
            }
            catch
            {
                Debug.Log("ERROR IN READING THE STIMULATION PARAMETERS");
                return -1;
            }
        }
        catch (TimeoutException)
        {
            Debug.Log("UNABLE TO READ SOMETHING");
            return -1;
        }
    }

    public static List<int> read_channels_PA_LIVE(SerialPort ser) //reading function while the stimulator is LIVE
    {
        /*
        :param ser: 
        :return: RETURN A LIST OF INTEGERS CORRESPONDING TO THE AMPLITUDES OF EACH CHANNEL. THIS IS ODNE WITH A DELAY OF 50 MS
        */
        ser.DiscardOutBuffer();
        ser.Write("RC\r");
        try
        {
            ser.ReadExisting();
        }
        catch (TimeoutException)
        {
        }
        System.Threading.Thread.Sleep(1000);
        ser.Write("RC\r"); //i MAKE THE READING TWICE, BECAUSE MOST OF THE TIMES THE FIRST ONE IS WRONG       
        try
        {
            channels_PA_b = ser.ReadExisting();
            Debug.Log(channels_PA_b);
            try
            {
                pFrom = channels_PA_b.IndexOf("\"C\":[") + "\"C\":[".Length;
                pTo = channels_PA_b.LastIndexOf("]");
                cutstring = channels_PA_b.Substring(pFrom, (pTo) - (pFrom));
                result = new List<int>(Array.ConvertAll(cutstring?.Split(','), int.Parse));
                Debug.Log(cutstring);
                Debug.Log(result[0]);
                Debug.Log(result[1]);
                Debug.Log(result[2]);
                Debug.Log(result[3]);
                return result;
            }
            catch
            {
                Debug.Log("ERROR IN READING THE STIMULATION PARAMETERS");
                return null;
            }
        }
        catch (TimeoutException)
        {
            Debug.Log("UNABLE TO READ SOMETHING");
            return null;
        }
    }

    public static List<int> read_channels_PW_LIVE(SerialPort ser) //reading function while the stimulator is in LIVE
    {
        /*
        :param ser: 
        :return: RETURN A LIST OF INTEGERS CORRESPONDING TO THE PW OF EACH CHANNEL. THIS IS ODNE WITH A DELAY OF 50 MS
        */
        ser.DiscardOutBuffer();
        ser.Write("RW\r");
        try
        {
            ser.ReadExisting();
        }
        catch (TimeoutException)
        {
        }
        System.Threading.Thread.Sleep(1000);
        ser.Write("RC\r"); //i MAKE THE READING TWICE, BECAUSE MOST OF THE TIMES THE FIRST ONE IS WRONG       
        try
        {
            channels_PW_b = ser.ReadExisting();
            Debug.Log(channels_PW_b);
            try
            {
                pFrom = channels_PW_b.IndexOf("\"PW\":[") + "\"PW\":[".Length;
                pTo = channels_PW_b.LastIndexOf("]");
                cutstring = channels_PW_b.Substring(pFrom, (pTo) - (pFrom));
                result = new List<int>(Array.ConvertAll(cutstring?.Split(','), int.Parse));
                Debug.Log(cutstring);
                Debug.Log(result[0]);
                Debug.Log(result[1]);
                Debug.Log(result[2]);
                Debug.Log(result[3]);
                return result;
            }
            catch
            {
                Debug.Log("ERROR IN READING THE STIMULATION PARAMETERS");
                return null;
            }
        }
        catch (TimeoutException)
        {
            Debug.Log("UNABLE TO READ SOMETHING");
            return null;
        }
    }


    public static int read_channels_PF_LIVE(SerialPort ser) //reading function while the stimulator is LIVE (THERE IS A DELAY OF 1 SEC)
    {
        /*
        :param ser: RETURNS AN INTEGER. THE FREQUECY
        :return:
        */
        ser.DiscardOutBuffer();
        ser.Write("RF\r");
        try
        {
            ser.ReadExisting();
        }
        catch (TimeoutException)
        {
        }
        System.Threading.Thread.Sleep(1000);
        ser.Write("RF\r"); //i MAKE THE READING TWICE, BECAUSE MOST OF THE TIMES THE FIRST ONE IS WRONG       
        try
        {
            channels_PF_b = ser.ReadExisting();
            Debug.Log(channels_PF_b);
            try
            {
                pFrom = channels_PF_b.IndexOf("\"F\":") + "\"F\":".Length;
                pTo = channels_PF_b.LastIndexOf("}");
                cutstring = channels_PF_b.Substring(pFrom, (pTo) - (pFrom));
                resultF = int.Parse(cutstring);
                Debug.Log(cutstring);
                Debug.Log(resultF);
                return resultF;
            }
            catch
            {
                Debug.Log("ERROR IN READING THE STIMULATION PARAMETERS");
                return -1;
            }
        }
        catch (TimeoutException)
        {
            Debug.Log("UNABLE TO READ SOMETHING");
            return -1;
        }
    }


}