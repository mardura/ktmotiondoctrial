using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ConstantStimulation : MonoBehaviour
{
    [DllImport("Dll Pipes Protocol")]
    private static extern int openAndWrite(string message);
    public Stimulator stimulator;
    bool stimulating = false;


    // Update is called once per frame
    void Update()
    {
        if (!stimulating)
        {
            if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (stimulator.stimulationChannels[i])
                    {
                        GeneralFunctions.Select_live_stimulation_parameters(stimulator.ser, i + 1, stimulator.PA[i], stimulator.PW_min[i]); //Start the stimulation of each enabled channel
                    }
                }
            }

            else
            {
                openAndWrite("9 " + stimulator.PW_min[0].ToString() + " " + "0 " + "\0"); // send pulsewidth constant
                System.Threading.Thread.Sleep(10);
                openAndWrite("1 1 1" + "\0"); // start constant stimulation
            }
            stimulating = true;
        }
    }
}
