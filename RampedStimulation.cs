using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Runtime.InteropServices;
using UnityEngine;

public class RampedStimulation : MonoBehaviour
{
    [DllImport("Dll Pipes Protocol")]
    private static extern int openAndWrite(string message);

  
    [HideInInspector] public Stimulator stimulator;
    [HideInInspector] public List<int> PW = new List<int>() { 10, 10, 10, 10 };

  


    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (stimulator.stim == Stimulator.StimulationDevice.KT_Motion)
        {

            for (int i = 0; i < stimulator.stimulationChannels.Count; i++)
            {
                if (stimulator.stimulationChannels[i])
                {
                    GeneralFunctions.Select_live_stimulation_parameters(stimulator.ser, stimulator.channels[i], stimulator.PA[i], PW[i]);
                }

            }
        }

        else
        {
            openAndWrite("9 " + PW.ToString() + " " + "0 " + "\0"); // send pulsewidth constant
            System.Threading.Thread.Sleep(10);
            openAndWrite("1 1 1" + "\0"); // start constant stimulation
        }
    }
}
